package com.training;

import com.training.person.Person;
import com.training.pets.Dragon;

public class InterfacesMain {
    public static void main(String[] args) {
        Feedable[] thingsWeCanFeed = new Feedable[5];

        Person horace = new Person("Horace");

        horace.setName("Horace O'Brien");

        Dragon smaug = new Dragon("Smaug");
        smaug.breathFire();

        thingsWeCanFeed[0] = horace;
        thingsWeCanFeed[1] = smaug;

        // loop over thingsWeCanFeed and feed them all
        for (Feedable thing : thingsWeCanFeed ) {
            if ( thing != null ) {
                thing.feed();
                if ( thing instanceof Dragon) {
                    ((Dragon) thing).breathFire();
                }
            }
        }
    }

}

