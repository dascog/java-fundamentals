package com.training;

import com.training.person.Person;
import com.training.pets.Dog;
import com.training.pets.Pet;

public class PersonMain {
    public static void main(String[] args) {
        Person myPerson = new Person();
        Pet dog = new Dog();
        dog.feed();
    }
}
