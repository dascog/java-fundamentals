package com.training.pets;
import com.training.Feedable;

public class Pet implements Feedable {

    public static int numPets = 0;

    private String name;

    private int numLegs;

    public Pet() {
        numPets++;
    }

    public void feed() {
        System.out.println("Feed generic pet some generic pet food");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumLegs() {
        return numLegs;
    }

    public void setNumLegs(int numLegs) {
        this.numLegs = numLegs;
    }

    public static int getNumPets() {
        return numPets;
    }

}