package com.training.pets;

public class Dragon extends Pet {
    public Dragon(String name) {
        this.setName(name);
    }

    public void feed() {
        System.out.println("Feed dragon some locusts");
    }

    public void breathFire() {
        System.out.println("FIIIRREEE !!!");
    }
}
