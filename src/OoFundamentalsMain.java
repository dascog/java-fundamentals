import com.training.pets.Dragon;
import com.training.pets.Pet;

public class OoFundamentalsMain {
    public static void main(String[] args) {
        Pet myPet = new Pet();
        myPet.setName("Xugo");
        myPet.setNumLegs(4);
        System.out.println(myPet.getNumLegs());
        System.out.println(myPet.getName());
        myPet.feed();

        Pet myDragon  = new Dragon("Mycroft");
        myDragon.setName("Mycroft");
        myDragon.setNumLegs(4);
        myDragon.feed();
        ((Dragon) myDragon).breathFire();

    }
}
